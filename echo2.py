#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import os
import threading

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 2222))
s.listen(1024)

def work():
    while True:
        conn,addr = s.accept()
        print ('connected:', addr)
        while True:
            data = conn.recv(1024)
            print(data)
            if data == b'close\r\n':
                break
            if not data: break
            conn.send(data)
        conn.close()


#p1 = threading.Thread(target=work, name="t1")
#p2 = threading.Thread(target=work, name="t2")

#p1.start()
#p2.start()

for _ in range(0, 11):
    p_ = threading.Thread(target=work, name="t1")
    p_.start()


















