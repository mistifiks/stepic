#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import os

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 2223))
s.listen(1024)

while True:
	conn,addr = s.accept()
	print ('connected:', addr)
	pid = os.fork();

	if pid > 0:
	    print('fork')

	if pid == 0:
	
		while True:

			data = conn.recv(1024)
			print(data)
			if data == b'close\r\n':
				break
			if not data: break
			conn.send(data)
		conn.close()

